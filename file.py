from __future__ import with_statement

import os
import sys


def read_file(fpath):
    with open(fpath, 'rb') as f:
        return f.read()


def dirname2(fn, n=1):
    fp = os.path.abspath(fn)
    for i in range(n):
        fp = os.path.dirname(fp)
    return fp


def pathjoin(p, n, *args):
    return os.path.join(dirname2(p, n), *args)


def inspath(p, n, *args):
    sys.path.insert(0, pathjoin(p, n, *args))
