from __future__ import print_function

import argparse
import unicodedata
from zipfile import ZipFile, ZipInfo


class MyZipFile(ZipFile):
    def __init__(self, *args, **kwargs):
        self.encoding = kwargs.pop('encoding', 'cp949')
        ZipFile.__init__(self, *args, **kwargs)

    def _extract_member(self, member, targetpath=None, pwd=None):
        if not isinstance(member, ZipInfo):
            member = self.getinfo(member)

        if isinstance(member.filename, bytes):  # 2.x
            filenameb = member.filename
        else:  # 3.x
            filenameb = member.filename.encode('cp437')
        member.filename = filenameb.decode(self.encoding)
        member.filename = unicodedata.normalize('NFC', member.filename)
        print(member.filename)
        ZipFile._extract_member(self, member, targetpath, pwd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--encoding', dest='encoding', action='store', default='cp949')
    parser.add_argument('filepaths', nargs='+', help='zip file')
    args = parser.parse_args()

    for fn in args.filepaths:
        print(fn)
        MyZipFile(fn, encoding=args.encoding).extractall()
