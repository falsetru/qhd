import unittest

from qhd.sound import nosound, quack


class BeepTest(unittest.TestCase):
    def test_nosound(self):
        nosound()

    @unittest.skip('quiet')
    def test_beep(self):
        play = quack()
        play()


if __name__ == '__main__':
    unittest.main()
