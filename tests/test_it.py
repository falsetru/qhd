import unittest

from qhd.it import slices, slicet, vslices, vslices2


class SlicesTest(unittest.TestCase):
    def test_slices_accept_joinfunc(self):
        self.assertEqual(
            list(slices('abcdefghi', 3, ''.join)),
            ['abc', 'def', 'ghi'])

    def test_slices_reserve_remain_elements(self):
        self.assertEqual(
            list(slices([1, 2, 3, 4], 3)),
            [(1, 2, 3), (4,)])

    def test_slices_for_empty_sequence(self):
        self.assertRaises(StopIteration, next, slices([], 2))

    def test_stop_condition_should_not_depend_on_joinfunc_result(self):
        """joinfunc parameter introduced bug."""
        self.assertEqual(
            list(slices('asdfg', 3, lambda x: 'x')),
            ['x', 'x'])


class SlicetTest(unittest.TestCase):
    def test_slicet_accept_joinfunc(self):
        self.assertEqual(
            list(slicet('abcdefghi', 3, ''.join)),
            ['abc', 'def', 'ghi'])

    def test_slicet_truncate_remain_elements(self):
        self.assertEqual(
            list(slicet([1, 2, 3, 4], 3)),
            [(1, 2, 3)])

    def test_stop_condition_should_not_depend_on_joinfunc_result(self):
        """joinfunc parameter introduced bug."""
        self.assertEqual(
            list(slicet('asdfg', 3, ','.join)),
            ['a,s,d'])


class VSlicesTest(unittest.TestCase):
    def test_vslices_divisible(self):
        # 1   4
        # 2   5
        # 3   6
        it = vslices([1, 2, 3, 4, 5, 6], 2)
        self.assertEqual(list(it), [(1, 4), (2, 5), (3, 6)])

    def test_vslices(self):
        # 1   4
        # 2   5
        # 3  None
        it = vslices([1, 2, 3, 4, 5], 2)
        self.assertEqual(list(it), [(1, 4), (2, 5), (3, None)])
        # 1 4 7
        # 2 5 .
        # 3 6 .
        self.assertEqual(
            list(vslices([1, 2, 3, 4, 5, 6, 7], 3)),
            [(1, 4, 7), (2, 5, None), (3, 6, None)])

    def test_vslices_1(self):
        # 1
        # 2
        # 3
        it = vslices([1, 2, 3], 1)
        self.assertEqual(list(it), [(1, ), (2, ), (3, )])

    def test_vslices_accept_fill_value(self):
        it = vslices([1, 2, 3, 4, 5], 2, fill=0)
        self.assertEqual(list(it), [(1, 4), (2, 5), (3, 0)])

    def test_empty(self):
        it = vslices([], 2)
        self.assertEqual(list(it), [])

    def test_known_bug(self):
        self.assertEqual(
            list(vslices([1, 2, 3, 4], 3)),
            [(1, 3), (2, 4)])


class VSlices2Test(unittest.TestCase):
    def test_vslice_even(self):
        # 1 2 3
        self.assertEqual([(1, 2, 3)], list(vslices2([1, 2, 3], 3)))

    def test_vslice_uneven(self):
        # 1 3 4
        # 2 . .
        self.assertEqual(
            list(vslices2([1, 2, 3, 4], 3)),
            [(1, 3, 4), (2, None, None)])
        # 1 4 6
        # 2 5 7
        # 3 . .
        self.assertEqual(
            list(vslices2([1, 2, 3, 4, 5, 6, 7], 3)),
            [(1, 4, 6), (2, 5, 7), (3, None, None)])
        # 1 4 7
        # 2 5 8
        # 3 6 .
        self.assertEqual(
            list(vslices2([1, 2, 3, 4, 5, 6, 7, 8], 3)),
            [(1, 4, 7), (2, 5, 8), (3, 6, None)])

    def test_sequence_is_not_enough_to_fill(self):
        # 1 2 .
        self.assertEqual(list(vslices2([1, 2], 3)), [(1, 2, None)])

    def test_fill_value(self):
        # 1 2 0
        self.assertEqual(list(vslices2([1, 2], 3, fill=0)), [(1, 2, 0)])


if __name__ == '__main__':
    unittest.main()
