# -*- coding: UTF-8 -*-

from __future__ import unicode_literals

import binascii
import unittest

from qhd.ibm1364 import decode, encode, line2rec
from qhd.it import slices


class ASCIIEncodingTest(unittest.TestCase):
    def test_spaces(self):
        self.assertEqual(encode(' \r\n'), b'\x40\x0d\x25')

    def test_symbols(self):
        self.assertEqual(encode('[]^'), b'\x70\x80\xb0')


class EBCDICEncodingTest(unittest.TestCase):
    def test_spaces(self):
        self.assertEqual(decode(b'\x40\x0d\x25'), ' \r\n')

    def test_symbols(self):
        self.assertEqual(decode(b'\x70\x80\xb0'), '[]^')


class MultibyteDecodingTest(unittest.TestCase):
    def test_all_hangul(self):
        self.assertEqual(decode(b'\x0e\xd0\x65\x8b\x69\x0f'), '한글')
        self.assertEqual(decode(b'\x0e\xac\xbc\x0f'), '샾')
        self.assertEqual(decode(b'\x0e\x88\x63\x0f'), '갂')
        self.assertEqual(decode(b'\x0e\x94\xe1\xac\xbc\x0f'), '더샾')

    def test_mixed(self):
        self.assertEqual(decode(b'\x81\x0e\xd0\x65\x8b\x69\x0f'), 'a한글')
        self.assertEqual(decode(b'\x0e\xd0\x65\x8b\x69\x0f\x81'), '한글a')
        self.assertEqual(decode(b'\x81\x82\x0e\xd0\x65\x8b\x69\x0f\x81'), 'ab한글a')

    def test_chinese_special_character(self):
        self.assertEqual(decode(b'\x0e\x54\x4f\x0f'), '大')
        self.assertEqual(decode(b'\x0e\x41\x79\x0f'), '\u2605')  # black star

    def test_jamo(self):
        tbl = '''
            ㄱ 8841 ㄲ 8c41 ㄳ 8444 ㄴ 9041 ㄵ 8446 ㄶ 8447 ㄷ 9441 ㄸ 9841
            ㄹ 9c41 ㄺ 844a ㄻ 844b ㄼ 844c ㄽ 844d ㄾ 844e ㄿ 844f ㅀ 8450
            ㅁ a041 ㅂ a441 ㅃ a841 ㅄ 8454 ㅅ ac41 ㅆ b041 ㅇ b441 ㅈ b841
            ㅉ bc41 ㅊ c041 ㅋ c441 ㅌ c841 ㅍ cc41 ㅎ d041 ㅏ 8461 ㅐ 8481
            ㅑ 84a1 ㅒ 84c1 ㅓ 84e1 ㅔ 8541 ㅕ 8561 ㅖ 8581 ㅗ 85a1 ㅘ 85c1
            ㅙ 85e1 ㅚ 8641 ㅛ 8661 ㅜ 8681 ㅝ 86a1 ㅞ 86c1 ㅟ 86e1 ㅠ 8741
            ㅡ 8761 ㅢ 8781 ㅣ 87a1'''.split()
        for ch, icode in slices(tbl, 2):
            icode = '0e' + icode + '0f'
            self.assertEqual(decode(binascii.a2b_hex(icode)), ch)


class MultibyteEncodingTest(unittest.TestCase):
    def test_all_hangul(self):
        self.assertEqual(encode('한글'), b'\x0e\xd0\x65\x8b\x69\x0f')
        self.assertEqual(encode('샾'), b'\x0e\xac\xbc\x0f')
        self.assertEqual(encode('갂'), b'\x0e\x88\x63\x0f')
        self.assertEqual(encode('더샾'), b'\x0e\x94\xe1\xac\xbc\x0f')

    def test_mixed(self):
        self.assertEqual(encode('a한글'), b'\x81\x0e\xd0\x65\x8b\x69\x0f')
        self.assertEqual(encode('한글a'), b'\x0e\xd0\x65\x8b\x69\x0f\x81')
        self.assertEqual(encode('ab한글a'), b'\x81\x82\x0e\xd0\x65\x8b\x69\x0f\x81')

    def test_chinese_special_character(self):
        self.assertEqual(encode('大'), b'\x0e\x54\x4f\x0f')
        self.assertEqual(encode('\u2605'), b'\x0e\x41\x79\x0f')  # black star

    def test_jamo(self):
        tbl = '''
            ㄱ 8841 ㄲ 8c41 ㄳ 8444 ㄴ 9041 ㄵ 8446 ㄶ 8447 ㄷ 9441 ㄸ 9841
            ㄹ 9c41 ㄺ 844a ㄻ 844b ㄼ 844c ㄽ 844d ㄾ 844e ㄿ 844f ㅀ 8450
            ㅁ a041 ㅂ a441 ㅃ a841 ㅄ 8454 ㅅ ac41 ㅆ b041 ㅇ b441 ㅈ b841
            ㅉ bc41 ㅊ c041 ㅋ c441 ㅌ c841 ㅍ cc41 ㅎ d041 ㅏ 8461 ㅐ 8481
            ㅑ 84a1 ㅒ 84c1 ㅓ 84e1 ㅔ 8541 ㅕ 8561 ㅖ 8581 ㅗ 85a1 ㅘ 85c1
            ㅙ 85e1 ㅚ 8641 ㅛ 8661 ㅜ 8681 ㅝ 86a1 ㅞ 86c1 ㅟ 86e1 ㅠ 8741
            ㅡ 8761 ㅢ 8781 ㅣ 87a1'''.split()
        for ch, icode in slices(tbl, 2):
            icode = '0e' + icode + '0f'
            self.assertEqual(encode(ch), binascii.a2b_hex(icode))


class Line2RecordTest(unittest.TestCase):
    def test_line2rec_should_pad_ebcdic_space(self):
        self.assertEqual(list(line2rec(['1'])), [b'\xf1'.ljust(80, b'\x40')])


if __name__ == '__main__':
    unittest.main()
