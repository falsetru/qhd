#!/usr/bin/env python
# coding: utf-8

import unittest

from qhd import txt


class TxtTest(unittest.TestCase):
    def test_dict_from_txt_empty_text(self):
        d = txt.dict_from_txt('')
        self.assertEqual({}, d)

    def test_dict_from_txt_normal(self):
        d = txt.dict_from_txt('a b\nc d')
        self.assertEqual({'a': 'b', 'c': 'd'}, d)

    def test_map_txt(self):
        self.assertEqual('b d', txt.map_txt('a c', {'a': 'b', 'c': 'd'}))

    def test_map_txt_ignore_error(self):
        self.assertEqual('b e', txt.map_txt('a e', {'a': 'b', 'c': 'd'}, strict=False))

    def test_map_txt_raise_error(self):
        self.assertRaises(KeyError, txt.map_txt, 'a e', {'a': 'b', 'c': 'd'}, strict=True)
        self.assertRaises(KeyError, txt.map_txt, 'a e', {'a': 'b', 'c': 'd'})


class MakingReplaceFunctionTest(unittest.TestCase):
    def test_mk_replace(self):
        replace = txt.mk_replace({'abc': 'xyz', '123': '789'})
        self.assertEqual('xyz789.', replace('abc123.'))


class TrimLinesTest(unittest.TestCase):
    def test_ltrimlines(self):
        self.assertEqual('a\nb', txt.ltrimlines('   a\n  b'))

    def test_ltrimlines_leave_non_leading_space(self):
        self.assertEqual('a\nb ', txt.ltrimlines('   a\n  b '))

    def test_ltrimlines_leave_blank_lines(self):
        self.assertEqual('a\n\nb', txt.ltrimlines('a\n\nb'))

    def test_rstrimlines_strip_trailing_spaces(self):
        self.assertEqual('hello\nworld', txt.rtrimlines('hello  \nworld'))

    def test_foo(self):
        self.assertEqual('hello\nworld', txt.trimlines('  hello  \n  world  '))


class TestAsAscii(unittest.TestCase):
    def test_as_ascii(self):
        self.assertEqual(u"''\"", txt.as_ascii(u"‘’”"))
        self.assertEqual(u'"', txt.as_ascii(u'″'))


if __name__ == '__main__':
    unittest.main()
