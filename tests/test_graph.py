import unittest

from qhd.graph import dijkstra, topological_sort


class TopologicalSortingTest(unittest.TestCase):
    def test_topological_sort_single_node(self):
        g = {'a': []}
        lst = list(topological_sort(g))
        self.assertEqual(['a'], lst)

    def test_topological_sort(self):
        g = {'a': ['b']}
        lst = list(topological_sort(g))
        self.assertEqual(['a', 'b'], lst)
        g = {'a': ['b', 'c'], 'c': ['b']}
        lst = list(topological_sort(g))
        self.assertEqual(['a', 'c', 'b'], lst)

    def test_cycle(self):
        g = {'a': ['b'], 'b': ['a']}
        it = topological_sort(g)
        self.assertRaises(ValueError, list, it)  # consume generator

    def test_island(self):
        g = {'a': ['b'], 'c': ['d']}
        lst = list(topological_sort(g))
        self.assertTrue(lst.index('a') < lst.index('b'))
        self.assertTrue(lst.index('c') < lst.index('d'))


class TestDijkstra(unittest.TestCase):
    def test_dijkstra(self):
        G = {
            'sf': {'la': 6, 'sd': 8},
            'la': {'sd': 2},
            'sd': {},
        }
        self.assertEqual(dijkstra(G, 'sf'), ({
            'la': 6,
            'sf': 0,
            'sd': 8,
        }, {
            'la': {'sf'},
            'sd': {'sf', 'la'},
        }))


if __name__ == '__main__':
    unittest.main()
