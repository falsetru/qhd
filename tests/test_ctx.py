from __future__ import with_statement

import unittest

from qhd.ctx import setting


class dummy:
    a = 0


class SettingContextManagerTest(unittest.TestCase):
    def test_setting(self):
        with setting(dummy, 'a', 1):
            self.assertEqual(1, dummy.a)

    def test_setting_restore_original_attribute_on_exit(self):
        with setting(dummy, 'a', 1):
            pass
        self.assertEqual(0, dummy.a)

    def test_setting_return_original_attribute(self):
        with setting(dummy, 'a', 1) as original:
            self.assertEqual(0, original)


if __name__ == '__main__':
    unittest.main()
