import operator
import unittest

from qhd.stat import decode, find_loop, uniq


class UniqTest(unittest.TestCase):
    def test_uniq(self):
        self.assertEqual([1, 2, 8, 9], uniq([1, 2, 8, 9, 8, 1, 2]))

    def test_uniq_with_key(self):
        self.assertEqual([1, 8], uniq([1, 2, 8, 9, 8, 1, 2], key=lambda x: x < 5))


class DecodeTest(unittest.TestCase):
    def test_decode(self):
        self.assertEqual(
            decode(3100000, operator.gt,
                   [(3000000, 3), (2000000, 2), (1000000, 1)], default=0),
            3)

    def test_decode_return_default_value(self):
        self.assertEqual(
            decode(100000, operator.gt,
                   [(3000000, 3), (2000000, 2), (1000000, 1)], default=0),
            0)


class TestFindLoop(unittest.TestCase):
    def test_find_loop(self):
        self.assertEqual(([], [1]), find_loop([1, 1, 1, 1]))
        self.assertEqual(([1], [2]), find_loop([1, 2, 2, 2, 2]))
        self.assertEqual(([1, 2, 3], []), find_loop([1, 2, 3]))

    def test_find_loop_accept_iterable(self):
        def _():
            yield 1
            while 1:
                yield 2
        self.assertEqual(([1], [2]), find_loop(_()))


if __name__ == '__main__':
    unittest.main()
