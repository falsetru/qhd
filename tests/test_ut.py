import unittest

from qhd import ut


def hello_iotestcase():
    print('Hello, IOTestCase!')


class IOTestCaseTest(ut.IOTestCase):
    def success_case(self):
        self.assertStdout('Hello, IOTestCase!\n', hello_iotestcase)

    def failure_case(self):
        self.assertStdout('Good bye!\n', hello_iotestcase)

    def test_success_case(self):
        result = unittest.TestResult()
        IOTestCaseTest('success_case').run(result)
        self.assertTrue(result.wasSuccessful())

    def test_failure_case(self):
        result = unittest.TestResult()
        IOTestCaseTest('failure_case').run(result)
        self.assertFalse(result.wasSuccessful())


if __name__ == '__main__':
    unittest.main()
