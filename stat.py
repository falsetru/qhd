from collections import defaultdict


def freq(it):
    f = defaultdict(int)
    for x in it:
        f[x] += 1
    return f


def dups(it):
    '''Filter duplicated items'''
    return dict((x, f) for x, f in freq(it).items() if f > 1)


def asis(x):
    return x


def uniqiter(it, key=asis):
    seen = set()
    for x in it:
        xk = key(x)
        if xk not in seen:
            yield x
            seen.add(xk)


def uniq(it, key=asis):
    return list(uniqiter(it, key))


def decode(x, pred, conds, default=None):
    for y, z in conds:
        if pred(x, y):
            return z
    return default


def find_loop(iterable):
    xs = []
    seen = set()
    for x in iterable:
        if x in seen:
            i = xs.index(x)
            return xs[:i], xs[i:]
        seen.add(x)
        xs.append(x)
    return xs, []
