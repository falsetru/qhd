'''
Excel (win32com) utility
'''

import os

from win32com.client import constants

# Run {sys.prefix}/lib/site-packages/win32com/client/makepy.py
#   Choose 'Microsoft Excel 11.0 Object Library (1.5)' or like. 'OK'


def save_as_csv(excel, path):
    # excel should be win32com.client-dispatched Excel.Appliation object
    path = os.path.abspath(path)
    try:
        os.unlink(path)
    except OSError as e:
        pass
    quiet(excel)
    excel.ActiveWorkbook.SaveAs(
            Filename=path,
            FileFormat=constants.xlCSV, CreateBackup=False,
            ConflictResolution=constants.xlLocalSessionChanges)


def replace_col(excel, col, old, new):
    quiet(excel)
    excel.Columns("%s:%s" % (col, col)).Select()
    excel.Selection.Replace(
        What=old, Replacement=new, LookAt=constants.xlPart,
        SearchOrder=constants.xlByRows, MatchCase=False, SearchFormat=False,
        ReplaceFormat=False)


def quiet(excel):
    excel.DisplayAlerts = False
