from __future__ import with_statement

from contextlib import contextmanager


@contextmanager
def using(x):
    yield x


def zero(*args, **kwargs):
    return 0


@contextmanager
def setting(obj, attr, x=zero):
    '''XXX Do not use this with nested'''
    attr_backup = getattr(obj, attr)
    try:
        setattr(obj, attr, x)
        yield attr_backup
    finally:
        setattr(obj, attr, attr_backup)


inside = using
