# http://effbot.org/zone/tkinter-busy.htm
import functools
from contextlib import contextmanager
from Tkinter import TclError


class BusyManager:

    def __init__(self, widget):
        self.toplevel = widget.winfo_toplevel()
        self.widgets = {}

    def busy(self, widget=None):

        # attach busy cursor to toplevel, plus all windows
        # that define their own cursor.

        if widget is None:
            w = self.toplevel  # myself
        else:
            w = widget

        if str(w) not in self.widgets:
            try:
                # attach cursor to this widget
                cursor = w.cget("cursor")
                if cursor != "watch":
                    self.widgets[str(w)] = (w, cursor)
                    w.config(cursor="watch")
            except TclError:
                pass

        for w in w.children.values():
            self.busy(w)

    def notbusy(self):
        # restore cursors
        for w, cursor in self.widgets.values():
            try:
                w.config(cursor=cursor)
            except TclError:
                pass
        self.widgets = {}


@contextmanager
def busy(widget):
    manager = BusyManager(widget)
    manager.busy()
    try:
        yield
    finally:
        manager.notbusy()


def busyfunc(widget):
    """decorator version of busy

    @busyfunc(widget)
    def on_changed(self, event):
        ...

    button['command'] = busyfunc(widget)(self.play)
    """
    def wrapper(f):
        @functools.wraps(f)
        def _(*args, **kwargs):
            with busy(widget):
                return f(*args, **kwargs)
        return _
    return wrapper
