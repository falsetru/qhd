#!/usr/bin/env python
from __future__ import print_function

import os
import sys


# exts = os.environ['PATHEXT'].split(os.pathsep)
def which(cmd, path=os.environ['PATH'],
          exts=('.exe', '.bat', '.cmd', '.com') if os.name == 'nt' else ('',)):
    candidates = ['%s%s' % (cmd, ext) for ext in exts]
    for p in path.split(os.pathsep):
        for fn in candidates:
            fp = os.path.join(p, fn)
            if os.path.exists(fp):
                return fp


if __name__ == '__main__':
    print(which(sys.argv[1]))
