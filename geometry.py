from Tkinter import BOTH, Button, Tk


def geometry():
    root = Tk()
    root.geometry('640x400')
    g = ['']

    def geo(e=0):
        g[0] = root.geometry()
        root.destroy()
    Button(root, command=geo).pack(expand=1, fill=BOTH)
    root.bind('<Return>', geo)
    root.mainloop()
    return g[0]
