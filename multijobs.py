# Unix only

from __future__ import print_function

import os
import subprocess


class DefaultHandler:
    def __init__(self):
        self.pid2job = {}
        self.pid2popen = {}

    def mkargs(self, job):
        return [job], {}

    def started(self, job, p, args, kwargs):
        self.pid2job[p.pid] = job
        self.pid2popen[p.pid] = p

    def start_failure(self, job, args, kwargs):
        print("Can't run", job)

    def success(self, pid):
        # print 'success', pid, ' '.join(self.pid2job[pid])
        pass

    def failure(self, pid, st):
        print('FAIL (status={1}): [{0}] '.format(' '.join(self.pid2job[pid]), st))

    def stopped(self, pid):
        # self.pid2popen[pid].wait()
        del self.pid2popen[pid]


class OutputLoggingHandler(DefaultHandler):
    def __init__(self, log_directory='.', filename_func=lambda x: '-'.join(x) + '.log'):
        DefaultHandler.__init__(self)
        try:
            os.makedirs(log_directory)
        except OSError:
            pass
        self.directory = log_directory
        self.filename_func = filename_func
        self.fobjs = {}

    def mkargs(self, job):
        f = open(os.path.join(self.directory, self.filename_func(job)), 'w')
        return [job], {'stdout': f, 'stderr': f}

    def started(self, job, p, args, kwargs):
        DefaultHandler.started(self, job, p, args, kwargs)
        self.fobjs[p.pid] = kwargs['stdout']

    def start_failure(self, job, args, kwargs):
        DefaultHandler.start_failure(self, job, args, kwargs)
        kwargs['stdout'].close()

    def stopped(self, pid):
        f = self.fobjs[pid]
        f.close()
        DefaultHandler.stopped(self, pid)


def run(jobs, windowsize, handler=None):
    assert windowsize > 0

    if not handler:
        handler = DefaultHandler()

    jobs = list(jobs)
    windowsize = min(windowsize, len(jobs))

    def run1():
        job = jobs.pop(0)
        args, kwargs = handler.mkargs(job)
        backup = subprocess._cleanup
        try:
            subprocess._cleanup = lambda: 0
            p = subprocess.Popen(*args, **kwargs)
        except OSError:
            handler.start_failure(job, args, kwargs)
            return 0
        finally:
            subprocess._cleanup = backup
        handler.started(job, p, args, kwargs)
        return 1

    active = 0
    while jobs and active < windowsize:
        for i in range(windowsize-active):
            active += run1()

    while jobs or active:
        if active:
            pid, st = os.wait()
            active -= 1
            if st == 0:
                handler.success(pid)
            else:
                handler.failure(pid, st)
            handler.stopped(pid)

        if jobs:
            while jobs:
                if run1() == 1:
                    active += 1
                    break


if __name__ == '__main__':
    jobs = [job.split() for job in '''
donothing
donothing
asdf
asdf
asdf
asdf
asdf
ls 12
'''.strip().splitlines()]
    run(jobs, 2, OutputLoggingHandler('log'))
