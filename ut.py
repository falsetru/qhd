import sys
import unittest
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

__unittest = 1


class IOTestCase(unittest.TestCase):
    def assertStdout(self, expected, f, *args, **kwargs):
        original = sys.stdout
        sys.stdout = StringIO()
        try:
            f(*args, **kwargs)
            self.assertEqual(expected, sys.stdout.getvalue())
        finally:
            sys.stdout.close()
            sys.stdout = original
