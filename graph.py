import sys
from collections import defaultdict
from heapq import heappop, heappush


def dijkstra(G, s):
    dist = defaultdict(lambda: sys.maxint)
    p = {}
    dist[s] = 0
    pq = [(0, s)]
    while pq:
        cost, start = heappop(pq)
        if cost > dist[start]:
            continue
        for stop, newcost in G[start].iteritems():
            newcost += cost
            if newcost < dist[stop]:
                dist[stop] = newcost
                heappush(pq, (newcost, stop))
                p[stop] = {start}
            elif newcost == dist[stop]:
                p[stop].add(start)
    return dist, p


def trails(p, v, s=()):
    '''
    p: predecessor map
    v: destination'''
    trail = (v,) + s
    if p[v] is None:
        yield trail
        return
    for u in p[v]:
        for x in trails(p, u, trail):
            yield x


def v2e(vs):
    return zip(vs, vs[1:])


def paths(G, p, s):
    path = {}
    for u in G:
        if u == s:
            continue
        path[u] = [v2e(vs) for vs in trails(p, u)]
    return path


# from http://code.activestate.com/recipes/119466/
#   "Recipe 119466: Dijkstra's algorithm for shortest paths"
def shortest_path(G, start, end):
    def flatten(L):       # Flatten linked list of form [0,[1,[2,[]]]]
        while len(L) > 0:
            yield L[0]
            L = L[1]

    q = [(0, start, ())]  # Heap of (cost, path_head, path_rest).
    visited = set()       # Visited vertices.
    while True:
        (cost, v1, path) = heappop(q)
        if v1 not in visited:
            visited.add(v1)
            if v1 == end:
                return list(flatten(path))[::-1] + [v1]
            path = (v1, path)
            for (v2, cost2) in G[v1].iteritems():
                if v2 not in visited:
                    heappush(q, (cost + cost2, v2, path))


# http://en.wikipedia.org/wiki/Topological_sorting
def topological_sort(G):
    S = [n for n in G if all(n not in dsts for dsts in G.values())]

    while S:
        n = S.pop()
        yield n
        if n in G:
            for m in G.pop(n):
                if all(m not in dsts for dsts in G.values()):
                    S.append(m)
    if G:
        raise ValueError('Graph has at least one cycle.')
