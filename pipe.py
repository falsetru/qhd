class Pipe:
    def __init__(self, value):
        self.value = value

    def __or__(self, f):
        if f is P:
            return self.value
        return Pipe(f(self.value))


P = Pipe(None)  # The beginning / Terminator
P.__or__ = Pipe

# P | 5 + 5 | range | len | P # -> 10
