#!/usr/bin/python
import subprocess
import sys

from qhd.geometry import geometry


if len(sys.argv) < 2:
    print('Usage: record output-filename-without-ext [sub-command]')
    print('')
    print('To Run vnc server:')
    print('x11vnc -quiet -localhost -viewonly -nopw -bg # -many')
    sys.exit(0)
if len(sys.argv) >= 3:
    sub = ' '.join(sys.argv[2:])
else:
    sub = ''
filename = sys.argv[1]

try:
    geo = geometry()
    args = [
        'flvrec.py',
        '-o', '%s.flv' % filename,                  # Output
    ]
    if geo:
        args.extend(['-C', geo])                # Clipping
    if sub:
        args.extend(['-S', sub])
    print(args)
    subprocess.call(args)
except KeyboardInterrupt:
    pass
