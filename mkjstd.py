'''
Initialize js-test-driver example directory.
(http://code.google.com/p/js-test-driver/)

Usage:
    mkdir js-project
    cd js-project
    python -m qhd.initjstd
'''
import getopt
import os
import sys


def make_batch(opt):
    with open('server.bat', 'w') as f:
        f.write('java -jar {jarpath} --port {port}\n'.format(**opt))
    with open('runtests.bat', 'w') as f:
        f.write('java -jar {jarpath} --tests all\n'.format(**opt))
    with open('main.bat', 'w') as f:
        f.write('python -m contest.main -f "*.js" java -jar {jarpath} --tests all\n'.format(**opt))
    os.chmod('server.bat', 0o755)
    os.chmod('runtests.bat', 0o755)
    os.chmod('main.bat', 0o755)


def make_conf(opt):
    with open('jsTestDriver.conf', 'w') as f:
        f.write('''\
server: http://localhost:{port}

load:
  - src/*.js
  - tests/*.js
'''.format(**opt))


def mkdir(path):
    try:
        os.makedirs(path)
    except OSError:
        pass


def make_example(opt):
    name = opt.get('name', 'example')
    mkdir('src')
    mkdir('tests')
    with open(os.path.join('src', name + '.js'), 'w') as f:
        f.write('function add(a, b) { return a + b; }')
    with open(os.path.join('tests', 'test_' + name + '.js'), 'w') as f:
        f.write('''\
{0}Test = TestCase("{0}Test");

{0}Test.prototype.testAdd = function() {{
    assertEquals(3, add(1, 2));
}}
'''.format(name.capitalize()))


def main():
    # default option
    port = 9876
    jarpath = 'JsTestDriver.jar'
    directory = '.'

    # parse option
    opts, args = getopt.getopt(sys.argv[1:], 'hp:j:')
    for o, a in opts:
        if o == '-h':
            print('Usage: python -m contest.initjstd [-h] [-p port] [-j js-test-driver-jar-path] [directory]')
            print('       default port = 9876')
            print('       default js-test-driver-jar-path = JsTestDriver-1.2.jar')
            print('       default directory = .')
            sys.exit(0)
        elif o == '-p':
            port = a
        elif o == '-j':
            jarpath = a
    if args:
        directory = args[0]

    # cd to given directory
    if directory != '.':
        jarpath = os.path.relpath(jarpath, directory)
        mkdir(directory)
        os.chdir(directory)
        name = os.path.basename(directory)
    else:
        name = os.path.basename(os.getcwd())

    # make
    opt = dict(port=port, jarpath=jarpath, name=name)
    make_batch(opt)
    make_conf(opt)
    make_example(opt)


if __name__ == '__main__':
    main()
