from itertools import islice
try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest


def slices(iterable, n, joinfunc=tuple):
    it = iter(iterable)
    while True:
        lst = tuple(islice(it, n))
        if not lst:
            break
        yield joinfunc(lst)


def slicet(iterable, n, joinfunc=tuple):
    '''slices, but truncate last one unless it's n-item(s).'''
    it = iter(iterable)
    while True:
        lst = tuple(islice(it, n))
        if len(lst) < n:
            break
        yield joinfunc(lst)


def vslices(seq, ncol, fill=None):
    assert ncol > 0
    q = (len(seq) - 1) // ncol + 1
    it = slices(seq, q)
    return zip_longest(*it, fillvalue=fill)


def vslices2(xs, n, fill=None):
    def part():
        q, r = divmod(len(xs), n)
        i = 0
        for j in range(n):
            x = q + (j < r)
            yield xs[i:i+x]
            i += x
    return zip_longest(*part(), fillvalue=fill)
