from __future__ import print_function

import win32com.client
from win32com.client import gencache


def pysupport(app, trace=False):
    tla = app._oleobj_.GetTypeInfo().GetContainingTypeLib()[0].GetLibAttr()
    if trace:
        print(tla)
    gencache.EnsureModule(tla[0], tla[1], tla[3], tla[4])


def main(appname):
    app = win32com.client.Dispatch(appname)
    pysupport(app, trace=True)
    app.Quit()


if __name__ == '__main__':
    import sys
    if len(sys.argv) != 2:
        print >>sys.stdout, "Exmaple Usage: python -m qhd.com Excel.Application"
        sys.exit(1)
    main(sys.argv[1])
