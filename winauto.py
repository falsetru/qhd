from __future__ import with_statement

import sys
from contextlib import closing
from cStringIO import StringIO

from pywinauto import application
from pywinauto.controls.common_controls import ListViewWrapper
from pywinauto.controls.HwndWrapper import _MetaWrapper
from win32api import mouse_event
from win32con import MOUSEEVENTF_LEFTDOWN, MOUSEEVENTF_LEFTUP
_MetaWrapper.str_wrappers['TsuiListView'] = ListViewWrapper
_MetaWrapper.str_wrappers['ComboLBox'] = ListViewWrapper
# _MetaWrapper.re_wrappers[re.compile('TsuiListView')] = ListViewWrapper


def connect(title, **kwargs):
    # assert isinstance(title, unicode)
    kwargs['title'] = title
    app = application.Application.connect(**kwargs)
    win = app[title]
    return win


def pconnect(title, pid):
    app = application.Application.connect(process=pid)
    win = app[title]
    return win


def printctrl(win):
    orig_sys_stdout = sys.stdout
    try:
        with closing(StringIO()) as m:
            sys.stdout = m
            win.PrintControlIdentifiers()
            print >>orig_sys_stdout, m.getvalue().decode('unicode-escape').encode(orig_sys_stdout.encoding, 'ignore')
    finally:
        sys.stdout = orig_sys_stdout


def click(x, y):
    mouse_event(MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
    mouse_event(MOUSEEVENTF_LEFTUP, x, y, 0, 0)
