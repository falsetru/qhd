'''
webbrowser module doesn't detect ff well (esp. on win32).
'''
import subprocess
import sys

_ff_path = '/usr/bin/firefox'


def register(path):
    global _ff_path
    _ff_path = path


if sys.platform == 'win32':
    import _winreg

    def _find_ff():
        '''
        HKEY_LOCAL_MACHINE/SOFTWARE/Mozilla/Mozilla Firefox/*/Main/PathToExe
        '''
        ff_keyname = r'SOFTWARE\Mozilla\Mozilla Firefox'
        ff_key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, ff_keyname)

        for i in range(0, sys.maxint):
            try:
                sub_keyname = _winreg.EnumKey(ff_key, i)
                break
            except WindowsError:
                break
        else:
            return

        key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, r'%s\%s\Main' % (
            ff_keyname, sub_keyname))
        for i in range(0, sys.maxint):
            try:
                name, value, typ = _winreg.EnumValue(key, i)
                if name == 'PathToExe':
                    return value
            except WindowsError:
                break
    _path = _find_ff()
    register(_path)


def open(url):
    global _ff_path
    subprocess.Popen([_ff_path, url], shell=False)


if __name__ == '__main__':
    import sys
    open(sys.argv[1])
