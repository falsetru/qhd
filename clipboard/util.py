from __future__ import print_function

import csv
import sys
from contextlib import closing

from qhd import clipboard
try:
    from cStringIO import StringIO
except ImportError:
    try:
        from StringIO import StringIO
    except ImportError:
        from io import StringIO


def excel_rows():
    txt = clipboard.get()
    txt = txt.encode(sys.stdout.encoding)
    with closing(StringIO(txt)) as f:
        for row in csv.reader(f, delimiter='\t'):
            yield row


def iter_convert(fn, trace=True):
    for row in excel_rows():
        out = fn(row)
        yield out
        if trace:
            print(out)


def convert(fn, joinstr='\n', trace=True):
    it = iter_convert(fn)
    output = joinstr.join(it)
    clipboard.set(output)
