'''
cf: Copy File contents
'''

import sys

from qhd import clipboard

if __name__ == '__main__':
    if len(sys.argv) == 1:
        contents = sys.stdin.read()
    else:
        filepath = sys.argv[1]
        with open(filepath, 'r') as f:
            contents = f.read()
    clipboard.set(contents)
