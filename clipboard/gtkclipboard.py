import gtk

_clipboard = gtk.Clipboard()


def get():
    return _clipboard.wait_for_text()


def set(txt):
    _clipboard.set_text(txt)
    _clipboard.store()
