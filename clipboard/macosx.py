from AppKit import NSPasteboard, NSString


def get():
    pb = NSPasteboard.generalPasteboard()
    return pb.readObjectsForClasses_options_((NSString,), None)[0]


def set(txt):
    pb = NSPasteboard.generalPasteboard()
    pb.clearContents()
    pb.writeObjects_((txt,))

# import subprocess
#
# def get():
#     p = subprocess.Popen(['pbpaste'], stdout=subprocess.PIPE)
#     p.wait()
#     return p.stdout.read()
#
# def set(txt):
#     p = subprocess.Popen(['pbpaste'], stdin=subprocess.PIPE)
#     p.stdin.write(data)
#     p.stdin.close()
#     p.wait()
