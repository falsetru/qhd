import win32clipboard
import win32con


def set(txt):
    win32clipboard.OpenClipboard()
    try:
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardText(txt)
    finally:
        win32clipboard.CloseClipboard()


def get():
    win32clipboard.OpenClipboard()
    try:
        txt = win32clipboard.GetClipboardData(win32con.CF_UNICODETEXT)
        return txt.partition(u'\0')[0]
    finally:
        win32clipboard.CloseClipboard()
