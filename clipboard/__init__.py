import sys

if sys.platform == 'win32':
    from .win32 import get, set
elif sys.platform == 'darwin':
    from .macosx import get, set
else:
    try:
        from gtkclipboard import get, set  # noqa
    except ImportError:
        pass
