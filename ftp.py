from ftplib import FTP

from qhd.ctx import setting


class FTPzOS(FTP):
    def putlines(self, remotefilename, f):
        with setting(self, 'voidcmd', self.sendcmd_typeb6):
            self.storlines("STOR %s" % remotefilename, f)

    def getlines(self, remotefilename, f):
        with setting(self, 'sendcmd', self.sendcmd_typeb6):
            self.retrlines(
                "RETR %s" % remotefilename,
                lambda line: f.write(line + '\n'))

    def sendcmd_typeb6(self, *args):
        if args and args[0] == 'TYPE A':
            args = ('TYPE B 6',) + args[1:]
        return FTP.sendcmd(self, *args)

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        self.quit()
