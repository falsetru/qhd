#!/usr/bin/env python
# coding: utf-8

from __future__ import with_statement

import re
from functools import partial

try:
    unicode
except NameError:
    unicode = str


def dict_from_txt(txt):
    mapping = {}
    for line in txt.splitlines():
        a, b = line.split()
        mapping[a] = b
    return mapping


def map_txt(txt, mapping, strict=True):
    if strict:
        getter = mapping.__getitem__
    else:
        def getter(x):
            return mapping.get(x, x)

    return re.sub(
        '\S+',
        lambda m: getter(m.group(0)),
        txt)


def mk_replace(tbl):
    patt = re.compile('|'.join(map(re.escape, tbl)))

    def replace(matched):
        return tbl[matched.group(0)]
    return partial(patt.sub, replace)


def as_ascii(txt):
    assert isinstance(txt, unicode), 'Argument must be unicode'
    txt = txt.replace(u'‘', u"'")
    txt = txt.replace(u'’', u"'")
    txt = txt.replace(u'“', u'"')
    txt = txt.replace(u'”', u'"')
    txt = txt.replace(u'″', u'"')
    txt = txt.replace(u'　', u' ')
    return txt


_spaces = r'[ \t]+'
_leading_space = re.compile('^' + _spaces, re.M)
_trailing_space = re.compile(_spaces + '$', re.M)
_leading_trailing_space = re.compile('^{0}|{0}$'.format(_spaces), re.M)


def _trimlines(txt, pattern):
    return pattern.sub('', txt)


def ltrimlines(txt):
    return _trimlines(txt, _leading_space)


def rtrimlines(txt):
    return _trimlines(txt, _trailing_space)


def trimlines(txt):
    return _trimlines(txt, _leading_trailing_space)
