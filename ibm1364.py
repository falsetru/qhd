from ibm1364_tbl import IBM1364_to_UNICODE, UNICODE_TO_IBM1364


# http://bugs.icu-project.org/trac/browser/icu/trunk/source/data/mappings/ibm-1364_P110-2007.ucm
def dump_unicode_to_ibm1364(filepath):
    for line in open(filepath):
        if not line.startswith('<U'):
            continue
        ucode, icode, flag = line.split()
        ucode = ucode[2:-1]
        print(r"'\u%s': b'%s'," % (ucode, icode))


def dump_ibm1364_to_unicode(filepath):
    print(r"0x3F: '\u001A',")
    # print r"b'\xFE\xFE': '\u001A',"
    for line in open(filepath):
        if not line.startswith('<U'):
            continue
        ucode, icode, flag = line.split()
        ucode = ucode[2:-1]
        if flag == '|0':
            print(r"0x%s: '\u%s'," % (icode.replace(r'\x', ''), ucode))


# dump_unicode_to_ibm1364('ibm-1364_P110-2007.ucm')
# dump_ibm1364_to_unicode('ibm-1364_P110-2007.ucm')

SO, SI = 0x0e, 0x0f
SOSI = SO, SI


def encode(u):
    result = bytearray()
    plen = 1
    for ch in u:
        b = UNICODE_TO_IBM1364[ch]
        clen = len(b)
        if plen != clen:
            result.append(SOSI[plen-1])
        result.extend(b)
        plen = clen
    if plen == 2:
        result.append(0x0f)
    return bytes(result)


def decode(e):
    def translate_each():
        wide = False
        code = 0
        for b in bytearray(e):
            if not wide:
                if b == SO:
                    wide = True
                else:
                    yield IBM1364_to_UNICODE[b]
            else:
                if b == SI:
                    wide = False
                else:
                    code = code << 8 | b
                    if code > 0xff:
                        yield IBM1364_to_UNICODE[code]
                        code = 0
    return ''.join(translate_each())


def line2rec(it, lrecl=80):
    for line in it:
        line = line.rstrip('\r\n')
        # X'40'  EBCDIC SPACE
        record = encode(line).ljust(lrecl, b'\x40')[:lrecl]
        yield record
