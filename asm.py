import doctest


def explain_register(asm):
    '''http://terzeron.net/wp/?p=952

    >>> explain_register('mov %esp, %ebp')
    'mov [stack pointer 32], [callee saved 32]'
    '''
    return (
        asm
        .replace('%ax',   '[return value 16]')
        .replace('%bx',   '[callee saved 16]')
        .replace('%cx',   '[4th arg 16]')
        .replace('%dx',   '[3rd arg 16]')
        .replace('%si',   '[2nd arg 16]')
        .replace('%di',   '[1st arg 16]')
        .replace('%bp',   '[callee saved 16]')
        .replace('%sp',   '[stack pointer 16]')
        .replace('%r8w',  '[5th arg 16]')
        .replace('%r9w',  '[6th arg 16]')
        .replace('%r10w', '[callee saved 16]')
        .replace('%r11w', '[used for linking 16]')
        .replace('%r12w', '[callee saved 16]')
        .replace('%r13w', '[callee saved 16]')
        .replace('%r14w', '[callee saved 16]')
        .replace('%r15w', '[callee saved 16]')
        .replace('%eax',  '[return value 32]')
        .replace('%ebx',  '[callee saved 32]')
        .replace('%ecx',  '[4th arg 32]')
        .replace('%edx',  '[3rd arg 32]')
        .replace('%esi',  '[2nd arg 32]')
        .replace('%edi',  '[1st arg 32]')
        .replace('%ebp',  '[callee saved 32]')
        .replace('%esp',  '[stack pointer 32]')
        .replace('%r8d',  '[5th arg 32]')
        .replace('%r9d',  '[6th arg 32]')
        .replace('%r10d', '[callee saved 32]')
        .replace('%r11d', '[used for linking 32]')
        .replace('%r12d', '[callee saved 32]')
        .replace('%r13d', '[callee saved 32]')
        .replace('%r14d', '[callee saved 32]')
        .replace('%r15d', '[callee saved 32]')
        .replace('%rax',  '[return value 64]')
        .replace('%rbx',  '[callee saved 64]')
        .replace('%rcx',  '[4th arg 64]')
        .replace('%rdx',  '[3rd arg 64]')
        .replace('%rsi',  '[2nd arg 64]')
        .replace('%rdi',  '[1st arg 64]')
        .replace('%rbp',  '[callee saved 64]')
        .replace('%rsp',  '[stack pointer 64]')
        .replace('%r8',   '[5th arg 64]')
        .replace('%r9',   '[6th arg 64]')
        .replace('%r10',  '[callee saved 64]')
        .replace('%r11',  '[used for linking 64]')
        .replace('%r12',  '[callee saved 64]')
        .replace('%r13',  '[callee saved 64]')
        .replace('%r14',  '[callee saved 64]')
        .replace('%r15',  '[callee saved 64]'))


doctest.testmod()
